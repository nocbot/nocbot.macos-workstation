# Ansible Role: macOS Workstation

This is an Ansible role that will install and configure various macOS components. Specifically, the responsibilities of this role are to:

* Install and configure Homebrew

## Requirements

Any pre-requisites that may not be covered by Ansible itself or this role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the `boto` package is required.

## Works with Ansible Galaxy

You can install this role with the `ansible-galaxy` command, and can run it directly from the git repository.

You should install it like this:

    ansible-galaxy install nocbot.nocbot.macos-workstation

Make sure you have write access to `/etc/ansible/roles/` since that is the default Ansible role installation path, or define your own Ansible role path by creating a `$HOME/.ansible.cfg` file with these contents:

    [defaults]
    roles_path = <path_to_your_preferred_role_location>

Change `<path_to_your_preferred_role_location>` to a directory you have write
access to. For testing with Vagrant, see the **Testing** section below.

See the [ansible-galaxy](http://docs.ansible.com/galaxy.html) documentation
for details.

## Role Variables

A description of the settable variables for this role should go here, including any variables that are in `defaults/main.yml`, `vars/main.yml`, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. `hostvars`, `group vars`, etc.) should be mentioned here as well.

### Default Variables

| Variable   | Default | Comments (type)  |
| :---       | :---    | :---             |
| `role_var` | -       | (scalar) PURPOSE |

#### Variable Notes

```console
    ## The following are the values of each option in the GUI
    # Start Screen Saver = 5
    #   Modifier = 0
    # Disable Screen Saver = 6
    #   Modifier = 0
    # Mission Control = 2
    #   Modifier = 0
    # Application Windows = 3
    #   Modifier = 0
    # Desktop = 4
    #   Modifier = 0
    # Dashboard = 7
    #   Modifier = 0
    # Notification Center = 12
    #   Modifier = 0
    # Launchpad = 11
    #   Modifier = 0
    # Put Display to Sleep = 10
    #   Modifier = 0
    # None = 1
    #   Modifier = 1048576
```

## Dependencies

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

## Example Playbook

See the test playbook in `tests/macos-workstation.yml`.

## Testing

    brew install vagrant virtualbox

### Configure Ansible Roles Path

Edit `roles_path` with your Ansible roles directory and put this snippet in `~/.ansible.cfg`

    [defaults]
    force_color = True
    roles_path = '/Volumes/USB Disk/Projects/nocbot/roles/'
    
    [ssh_connection]
    ssh_args = -o ForwardAgent=yes -o ControlMaster=auto -o ControlPersist=60s
    pipelining = True
    retries = 1

### Configure sudo for Vagrant NFS mounts

#### macOS

Put the following in `/etc/sudoers.d/vagrant`.

    Cmnd_Alias VAGRANT_EXPORTS_ADD = /usr/bin/tee -a /etc/exports
    Cmnd_Alias VAGRANT_NFSD = /sbin/nfsd restart
    Cmnd_Alias VAGRANT_EXPORTS_REMOVE = /usr/bin/sed -E -e /*/ d -ibak /etc/exports
    %admin ALL=(root) NOPASSWD: VAGRANT_EXPORTS_ADD, VAGRANT_NFSD, VAGRANT_EXPORTS_REMOVE

## License

MIT License

## Author Information

`@tracphil` (maintainer)
