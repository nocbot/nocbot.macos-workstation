## Contributing

[fork]: https://github.com/nocbotio/nocbot-role/fork
[pr]: https://github.com/nocbotio/nocbot-role/compare
[best_practice]: http://docs.ansible.com/ansible/latest/playbooks_best_practices.html
[code-of-conduct]: CODE_OF_CONDUCT.md
[good-first-issue-search]: https://github.com/nocbotio/nocbot-role/labels/help%20wanted

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:

We're thrilled that you'd like to contribute to this project. Your help is essential for keeping it great.

#### Table Of Contents

[Code of Conduct](#code-of-conduct)

This project and everyone participating in it is governed by the [Nocbot Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [abuse@nocbot.io](mailto:abuse@nocbot.io).

[I don't want to read this whole thing, I just have a question!!!](#i-dont-want-to-read-this-whole-thing-i-just-have-a-question)

> **Note:** Please don't file an issue to ask a question. You'll get faster results by using the resources below.

We have an official message board with a detailed FAQ and where the community chimes in with helpful advice if you have questions.

- [Discuss, the official Nocbot message board](https://discuss.nocbot.io)
- [Atom FAQ](https://discuss.nocbot.io/c/faq)

If chat is more your speed, you can join the Nocbot Slack team:

- [Join the Nocbot Slack Team](https://nocbot-slack.herokuapp.com/)
    - Even though Slack is a chat service, sometimes it takes several hours for community members to respond &mdash; please be patient!
    - Use the `#nocbot` channel for general questions or discussion about Atom
    - There are many other channels available, check the channel list

## Submitting a pull request(#submit-pull-request)

1. [Fork][fork] and clone the repository
1. Configure and install the dependencies: `pip install -r requirements.txt`
1. Make sure the tests pass on your machine: `cd tests && vagrant up`, note: these tests also apply the linter, so no need to lint separately
1. Create a new branch: `git checkout -b my-branch-name`
1. Make your change, add tests, and make sure the tests still pass
1. Push to your fork and [submit a pull request][pr]
1. Pat your self on the back and wait for your pull request to be reviewed and merged.

Here are a few things you can do that will increase the likelihood of your pull request being accepted:

- Follow the [best_practice guide][best_practice] which is using standard. Any linting errors should be shown when running `ansible-lint playbook.yml|roledirectory ...`
- Write and update tests.
- Keep your change as focused as possible. If there are multiple changes you would like to make that are not dependent upon each other, consider submitting them as separate pull requests.
- Write a [good commit message](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html).

Work in Progress pull requests are also welcome to get feedback early on, or if there is something that blocked you.

### Just starting out? Looking for how to help?(#just-starting-out)

Use [this search][good-first-issue-search] to find roles that have issues marked with the `good-first-issue` label.

## Resources(#resources)

- [How to Contribute to Open Source](https://opensource.guide/how-to-contribute/)
- [Using Pull Requests](https://help.github.com/articles/about-pull-requests/)
- [GitHub Help](https://help.github.com)
