#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o nounset

repo="https://bitbucket.org/nocbot/nocbot.macos-workstation.git"
target_dir="$HOME/Projects/nocbot/roles/"

if ! xcode-select --print-path &> /dev/null; then
    echo "Installing Command Line Tools"

    # Prompt user to install the Command Line Tools
    xcode-select --install &> /dev/null

    # Wait until the Command Line Tools are installed
    until xcode-select --print-path &> /dev/null; do
      sleep 5
    done

    echo "Done"
else
    echo "Command Line Tools are already installed"
fi

if test ! "$(which ansible)"; then
    echo "Installing Ansible"
    sudo easy_install pip
    sudo pip install ansible
    echo "Done"
else
    echo "Ansible is already installed"
fi

if [[ ! -d "${target_dir}/nocbot.macos-workstation" ]]; then
    echo "Downloading & extracting nocbot.macos-workstation repository"
    mkdir -p ${target_dir}
    cd ${target_dir}
    git clone ${repo}
else
    echo "The nocbot.macos-workstation repository is already in place"
fi

cd ..

if [[ ! -f macos-workstation-playbook.yml ]]; then
    cp roles/nocbot.macos-workstation/macos-workstation-playbook-example.yml macos-workstation-playbook.yml
    echo "Creating macos-workstation-playbook.yml"
fi

echo "Running the playbook"
ansible-playbook -i "localhost," -c local macos-workstation-playbook.yml --ask-become-pass

echo ""
echo " Finished!"
echo ""
echo "Add applications that you want installed to the macos-workstation-playbook.yml playbook"
